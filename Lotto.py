# random generaattorin todentaminen
import random
# Hello World teksin tekeminen ja tulostaminen tarkistaakseen ohjelman toimivuus
msg = "Hello World"
print(msg)

# Lukujen tarkistus kahdesta listasta (käyttäjän antamista luvuista)
def lukujentarkistus(lukutaulukko, randomnumerot):
    # Jos lukutaulukon arvot ovat samat, sitten tämä tapahtuu
    if lukutaulukko == randomnumerot:
        print("Onneksi olkoon, voitit päävoiton omilla numeroillasi!")
        print("Numerosi ovat", lukutaulukko)
        print("Lottorivi on", randomnumerot, "\n")
    # Jos luvut eivät ole samat, niin tämä tapahtuu
    else:
        # Katsotaan, mitkä luvut ovat taulukossa samoja
        oikein = [i for i in lukutaulukko if i in randomnumerot]
        print("Et voittanut päävoittoa omilla numeroillasi, parempi onni ensikerralla")
        print("Numerosi ovat", lukutaulukko)
        print("Lottorivi on", randomnumerot)
        print("Sait nämä numerot oikein: ", oikein, "\n")

# Lukujen tarkistus kahdesta listasta (käyttäjän luvut arvottuna)
def randomlukujentarkistus(kayttajarandom, randomnumerot):
    # Jos lukutaulukon arvot ovat samat, sitten tämä tapahtuu
    if kayttajarandom == randomnumerot:
        print("Onneksi olkoon, voitit päävoiton arvotuilla numeroilla!")
        print("Numerosi ovat", kayttajarandom)
        print("Lottorivi on", randomnumerot, "\n")
    # Jos luvut eivät ole samat, niin tämä tapahtuu
    else:
        # Katsotaan, mitkä luvut ovat taulukossa samoja
        oikein1 = [i for i in kayttajarandom if i in randomnumerot]
        print("Et voittanut päävoittoa arvotuilla numeroilla, parempi onni ensikerralla")
        print("Numerosi ovat", kayttajarandom)
        print("Lottorivi on", randomnumerot)
        print("Sait nämä numerot oikein: ", oikein1, "\n")

# Funktio kayttajanluvut
def kayttajanluvut():
    # Tyhjä taulukko
    lukutaulukko = []
    # While loopissa niin kauan ennenkuin on saatu 7 numeroa, jotka täyttävät ehdot
    while len(lukutaulukko) < 7:
        # Numeron syöttö
        numerot = input("Anna numeroita väliltä 1-40: ")
        # Numeron tarkistus onko int muodossa vai ei ja jos ei ole, niin sitten syötetään uusi numero sen tilalle
        try:
            numerot = int(numerot)
        except:
            print("Virhe, annetun luvun pitää olla numero")
            continue
        # Numeron täytyy olla välillä 1-40
        if 1 <= numerot <= 40:
            # Numero taulukkoon, jos se ei siellä vielä ole
            if numerot not in lukutaulukko:
                lukutaulukko.append(numerot)
            # Jos valitset saman numeron, tulee virhe ja valitset uuden numeron, jota et ole vielä valinnut
            else:
                print("Virhe, olet jo valinnut tämän luvun")
        else:
            # Jos numero ei ole oikealla lukuvälillä, tulee virhe ja valitset uudestaan lukuja, kunnes ne ovat oikealla lukuvälillä
            print("Virhe, lukusi ei ole oikealla lukuvälillä")
    # Palautetaan taulukko
    return lukutaulukko

# Käyttäjän luvut arvottuna valittuna oikeasta lukuvälistä ja 7 numeroa
def kayttajanluvutrandom():
    return random.sample(range(1, 40), 7)

# Lotto luvut arvottuna valittuna oikeasta lukuvälistä ja 7 numeroa
def voittavatnumerot():
    return random.sample(range(1, 40), 7)
# While loop
while True:
    # Valitsemis ohjeet
    print("Valitsemalla 1 saat käyttöohjeet ensin lottoon ja pelaat omilla numeroilla tai"
    "\nValitsemalla 2 pääset suoraan pelaamaan omilla numeroilla tai"
    "\nValitsemalla 3 lopetat pelaamisen tai"
    "\nValitsemalla 4 saat käyttöohjeet lottoon ja numerosi arvotaan tai"
    "\nValitsemalla 5 pääset suoraan pelaamaan ja numerosi arvotaan")
    # Valinnat
    a=input("Tee valinta, 1 tai 2 tai 3 tai 4 tai 5: ")
    # Jos valitsit 1, tämä tapahtuu
    if a=="1":
        print("Pelaat lottoa, sinun tulee antaa 7 eri numeroa väliltä 1-40")
        print("Jos kaikki numerosi ovat oikein, voitat päävoiton")
        lukutaulukko = kayttajanluvut()
        randomnumerot = voittavatnumerot()
        lukujentarkistus(lukutaulukko, randomnumerot)
    # Jos valitsit 2, tämä tapahtuu
    elif a=="2":
        lukutaulukko = kayttajanluvut()
        randomnumerot = voittavatnumerot()
        lukujentarkistus(lukutaulukko, randomnumerot)
    # Jos valitsit 3, tämä tapahtuu
    elif a=="3":
        print("Kiitos pelaamisesta")
        break
    # Jos valitsit 4, tämä tapahtuu
    elif a=="4":
        print("Pelaat lottoa, sinulle arvotaan 7 numeroa väliltä 1-40")
        print("Jos kaikki numerosi ovat oikein, voitat päävoiton")
        kayttajarandom = kayttajanluvutrandom()
        randomnumerot = voittavatnumerot()
        randomlukujentarkistus(kayttajarandom, randomnumerot)
    # Jos valitsit 5 tämä tapahtuu
    elif a=="5":
        kayttajarandom = kayttajanluvutrandom()
        randomnumerot = voittavatnumerot()
        randomlukujentarkistus(kayttajarandom, randomnumerot)
    # Jos et valinnut 1-5, vaan valitsit jotain muuta, niin sitten tulee virhe ja saat valita niinkauan kunnes valinta on oikealta väliltä
    else:
        print("\nVirhe, virheellinen syöte, syötä oikealla välillä oleva numero\n")
        
